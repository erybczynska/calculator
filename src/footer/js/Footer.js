import React, {Component} from 'react';
import '../style/Footer.css';
import {Grid, Col, Row} from 'react-bootstrap';

class Footer extends Component {
  render() {
    return (
        <footer>
          <div className="Footer">
            <Grid>
              <Row className="show-grid">
                <Col md={4}>
                  <p>Social</p>
                </Col>
                <Col md={4}>
                  <p>About us</p>
                </Col>
                <Col md={4}>
                  <p>Contact</p>
                </Col>
              </Row>
            </Grid>
          </div>
        </footer>
    );
  }
}

export default Footer;