import React, {Component} from 'react';
import '../style/ProductsList.css';
import SingleProduct from './SingleProduct';
import {Table} from 'react-bootstrap';
import Amount from './Amount';

class ProductsList extends Component {

  render() {
    const removeCallback = this.props.removeCallback;
    const products = this.props.products;
    const productsList = products.map(
        product => <SingleProduct key={product.name} value={product}
                                  removeCallback={removeCallback}/>);

    return (
        <Table responsive>
          <thead>
          <tr>
            <th>Product</th>
            <th>Kcal</th>
            <th>Protein</th>
            <th>Carbs</th>
            <th>Fat</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          {productsList}
          <Amount product={products}/>
          </tbody>
        </Table>
    );
  }
}

export default ProductsList;