import React, {Component} from 'react';
import '../style/ProductsCalculator.css';
import ProductsList from './ProductsList';
import {Grid, Col, Row} from 'react-bootstrap';
import Autocomplete from 'react-autocomplete';
import AutocompleteUtils from './AutocompleteUtils';
class ProductsCalculator extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      amountInput: '100',
      nameInput: ''
    };

    this.handleNameInput = this.handleNameInput.bind(this);
    this.addNewProduct = this.addNewProduct.bind(this);
    this.handleAmountInput = this.handleAmountInput.bind(this);
    this.remove = this.remove.bind(this);
  }

  handleAmountInput(event) {
    this.setState({amountInput: event.target.value});
  }

  addNewProduct(event) {
    event.preventDefault();
    const currentName = this.state.nameInput;
    const newProduct = AutocompleteUtils.getProductByName(currentName);
    if (newProduct) {
      newProduct.amount = this.state.amountInput;
      this.setState((prevState, props) => ({
        data: prevState.data.concat([newProduct]),
        amountInput: '',
        nameInput: ''
      }));
    }
  }

  handleNameInput(event) {
    this.setState({nameInput: event.target.value});
  }

  remove(name) {
    this.setState((prevState, props) => ({
      data: prevState.data.filter(product => product.name !== name)
    }));
  }

  render() {
    return (
        <main>
          <div className="ProductsCalculator">
            <Grid>
              <Row className="show-grid">
                <Col xs={6} md={4}>
                  <h2>Get your calories under control.</h2>
                  <form onSubmit={this.addNewProduct}>
                    <p>Product:</p>
                    <Autocomplete
                        value={this.state.nameInput}
                        inputProps={{
                          name: "Products",
                          id: "products-autocomplete"
                        }}
                        items={AutocompleteUtils.getProducts()}
                        getItemValue={(item) => item.name}
                        shouldItemRender={AutocompleteUtils.matchProductToTerm}
                        sortItems={AutocompleteUtils.sortProducts}
                        onChange={(event, value) => this.setState(
                            {nameInput: value})}
                        onSelect={value => {
                          this.setState({nameInput: value});
                        }}
                        renderItem={(item, isHighlighted) => (
                            <div className={isHighlighted ?
                                'autocomplete-highlighted-item'
                                : 'autocomplete-item'}
                                 key={item.abbr}
                            >{item.name}</div>
                        )}
                    /> <br />
                    <p>Amount (grams):</p>
                    <input type="number" value={this.state.amountInput}
                           onChange={this.handleAmountInput} min="1"
                           max="1000"/><br />
                    <input type="submit" value="Add new item"/>
                  </form>
                </Col>
                <Col xs={12} md={8}>
                  <h1>What did you eat today?</h1>
                  <ProductsList products={this.state.data}
                                removeCallback={this.remove}/>
                </Col>
              </Row>
            </Grid>
          </div>
        </main>
    );
  }
}

export default ProductsCalculator;
