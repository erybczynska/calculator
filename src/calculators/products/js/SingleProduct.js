import React, {Component} from 'react';
import '../style/SingleProduct.css';

class SingleProduct extends Component {

  render() {
    function calculate(value, amount) {
      return ((value * amount) / 100).toFixed(2);
    }

    const removeCallback = this.props.removeCallback;
    const amount = this.props.value.amount;
    const name = this.props.value.name;
    const calories = calculate(this.props.value.calories, amount);
    const proteins = calculate(this.props.value.proteins, amount);
    const fats = calculate(this.props.value.fats, amount);
    const carbs = calculate(this.props.value.carbs, amount);

    return (
        <tr>
          <td>{name}</td>
          <td>{calories} kcal</td>
          <td>{proteins} g</td>
          <td>{carbs} g</td>
          <td>{fats} g</td>
          <td>
            <button className="close" onClick={() => removeCallback(name)}>x
            </button>
          </td>
        </tr>
    );
  }

}

export default SingleProduct;
