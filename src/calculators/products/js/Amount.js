import React, {Component} from 'react';

class Amount extends Component {

  render() {
    const product = this.props.product;
    const kcalSum =
        product
        .map(product => parseFloat((product.calories * product.amount) / 100))
        .reduce(function (a, b) {
          return a + b;
        }, 0);

    const proteinSum =
        product
        .map(product => parseFloat((product.proteins * product.amount) / 100))
        .reduce(function (a, b) {
          return a + b;
        }, 0);

    const fatsSum =
        product
        .map(product => parseFloat((product.fats * product.amount) / 100))
        .reduce(function (a, b) {
          return a + b;
        }, 0);

    const carbsSum =
        product
        .map(product => parseFloat((product.carbs * product.amount) / 100))
        .reduce(function (a, b) {
          return a + b;
        }, 0);

    return (
        <tr className="total">
          <td>{kcalSum > 0 && <span >Total:</span>}</td>
          <td>{kcalSum > 0 && <span>{kcalSum.toFixed(2)} kcal</span>}</td>
          <td>{proteinSum > 0 && <span>{proteinSum.toFixed(2)} g </span>}</td>
          <td>{carbsSum > 0 && <span>{carbsSum.toFixed(2)} g</span>}</td>
          <td>{fatsSum > 0 && <span>{fatsSum.toFixed(2)} g</span>}</td>
          <td></td>
        </tr>
    );
  }
}

export default Amount;