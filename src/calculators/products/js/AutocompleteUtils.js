class AutocompleteUtils {

  static data = [
    {name: "gooseberry", calories: 41, proteins: 0.8, fats: 0.2, carbs: 0.2},
    {name: "water-melon", calories: 36, proteins: 0.6, fats: 0.1, carbs: 8.4},
    {name: "pineapple", calories: 54, proteins: 0.4, fats: 0.2, carbs: 12.4},
    {name: "avokado", calories: 160, proteins: 2, fats: 15.3, carbs: 4.1},
    {name: "banana", calories: 95, proteins: 1, fats: 0.3, carbs: 23.5},
    {name: "baguette", calories: 283, proteins: 8.7, fats: 1.7, carbs: 57.2},
    {name: "becon", calories: 110, proteins: 20.3, fats: 2.6, carbs: 1.3},
    {name: "broccoli", calories: 27, proteins: 3, fats: 0.4, carbs: 2.7},
    {name: "peach", calories: 46, proteins: 1, fats: 0.2, carbs: 10},
    {name: "beet", calories: 38, proteins: 1.8, fats: 0.1, carbs: 7.3},
    {name: "onion", calories: 30, proteins: 1.4, fats: 0.4, carbs: 5.2},
    {name: "bread", calories: 248, proteins: 6.1, fats: 1.3, carbs: 52.1},
    {name: "cola", calories: 42, proteins: 0, fats: 0, carbs: 10.5},
    {name: "sugar", calories: 404, proteins: 0, fats: 0, carbs: 99.8},
    {name: "courgette", calories: 15, proteins: 1.2, fats: 0.1, carbs: 2.2},
    {name: "chocolate", calories: 549, proteins: 9.8, fats: 32.8, carbs: 54},
    {name: "cod", calories: 78, proteins: 17.7, fats: 0.7, carbs: 0},
    {name: "jam", calories: 153, proteins: 0.4, fats: 0.2, carbs: 36.9},
    {name: "plum", calories: 45, proteins: 0.6, fats: 0.2, carbs: 10.1},
    {name: "salmon", calories: 201, proteins: 19.9, fats: 13.6, carbs: 0},
    {name: "potatoes", calories: 77, proteins: 1.9, fats: 0.1, carbs: 18.9},
    {name: "beef", calories: 154, proteins: 20.8, fats: 7.9, carbs: 0},
    {name: "beef", calories: 154, proteins: 20.8, fats: 7.9, carbs: 0},
    {name: "cottage cheese", calories: 175, proteins: 17.7, fats: 10.1, carbs: 3.4},
    {name: "strawberry", calories: 28, proteins: 0.7, fats: 0.4, carbs: 5.4},
    {name: "tuna", calories: 137, proteins: 23.7, fats: 4.6, carbs: 0},
    {name: "ham", calories: 261, proteins: 18, fats: 21.3, carbs: 0},
    {name: "spinach", calories: 16, proteins: 2.6, fats: 0.4, carbs: 0.4},
    {name: "orange juice", calories: 47, proteins: 0.7, fats: 0.2, carbs: 10.7},
    {name: "mozzarella", calories: 254, proteins: 24, fats: 16, carbs: 3.5},
    {name: "cheese", calories: 316, proteins: 27.9, fats: 22.9, carbs: 0.1},
    {name: "salad", calories: 14, proteins: 0.9, fats: 0.1, carbs: 1.8},
    {name: "salami", calories: 540, proteins: 21.9, fats: 50.6, carbs: 0.8},
    {name: "rice", calories: 344, proteins: 6.7, fats: 0.7, carbs: 76.8},
    {name: "oatmeal", calories: 379, proteins: 13.2, fats: 6.5, carbs: 57.6},
    {name: "tomato", calories: 15, proteins: 0.9, fats: 0.2, carbs: 2.4},
    {name: "orange", calories: 44, proteins: 0.9, fats: 0.2, carbs: 9.4},
    {name: "mushroom", calories: 17, proteins: 2.7, fats: 0.4, carbs: 0.6},
    {name: "pepper", calories: 27, proteins: 1, fats: 0.2, carbs: 5.4},
    {name: "cashew", calories: 574, proteins: 15.3, fats: 46.4, carbs: 29.7},
    {name: "cashew", calories: 574, proteins: 15.3, fats: 46.4, carbs: 29.7},
    {name: "olive", calories: 882, proteins: 0, fats: 99.6, carbs: 0},
    {name: "cucumber", calories: 13, proteins: 0.7, fats: 0.1, carbs: 2.4},
    {name: "flour", calories: 322, proteins: 11.3, fats: 2.3, carbs: 63},
    {name: "milk", calories: 61, proteins: 3.3, fats: 3.2, carbs: 4.8},
    {name: "honey", calories: 324, proteins: 0.3, fats: 0, carbs: 79.7},
    {name: "butter", calories: 735, proteins: 0.7, fats: 82.5, carbs: 0.7},
    {name: "carrot", calories: 27, proteins: 1, fats: 0.2, carbs: 5.1},
    {name: "pasta", calories: 371, proteins: 13, fats: 1.5, carbs: 71.5},
    {name: "chicken", calories: 158, proteins: 18.6, fats: 9.3, carbs: 0},
    {name: "kiwi", calories: 56, proteins: 0.9, fats: 0.5, carbs: 11.8},
    {name: "sausage", calories: 209, proteins: 17.6, fats: 15.6, carbs: 0},
    {name: "ketchup", calories: 93, proteins: 1.8, fats: 1, carbs: 19},
    {name: "coffee", calories: 2, proteins: 0.2, fats: 0, carbs: 0.3},
    {name: "sausage", calories: 209, proteins: 17.6, fats: 15.6, carbs: 0},
    {name: "millet", calories: 346, proteins: 10.5, fats: 2.9, carbs: 68.4},
    {name: "yoghurt", calories: 60, proteins: 4.3, fats: 2, carbs: 6.2},
    {name: "egg", calories: 139, proteins: 12.5, fats: 9.7, carbs: 0.6},
    {name: "apple", calories: 46, proteins: 0.4, fats: 0.4, carbs: 10.1},
    {name: "turkey", calories: 129, proteins: 17, fats: 6.8, carbs: 0},
    {name: "biscuit", calories: 473, proteins: 6.4, fats: 17.2, carbs: 65.8},
    {name: "pear", calories: 54, proteins: 0.6, fats: 0.2, carbs: 12.3},
    {name: "musli", calories: 471, proteins: 10.1, fats: 19.8, carbs: 59.1},
    {name: "fries", calories: 289, proteins: 3.5, fats: 14, carbs: 33.3},
    {name: "bean", calories: 288, proteins: 21.4, fats: 1.6, carbs: 45.9}
  ];

  static styles = {
    item: {
      padding: '2px 6px',
      cursor: 'default'
    },

    highlightedItem: {
      color: 'white',
      background: 'hsl(200, 50%, 50%)',
      padding: '2px 6px',
      cursor: 'default',
      position: 'absolute'
    },

    menu: {
      border: 'solid 1px #ccc'
    }
  };

  static matchProductToTerm(state, value) {
    return (
        state.name.toLowerCase().indexOf(value.toLowerCase()) !== -1
    )
  }

  static sortProducts(a, b, value) {
    const aLower = a.name.toLowerCase();
    const bLower = b.name.toLowerCase();
    const valueLower = value.toLowerCase();
    const queryPosA = aLower.indexOf(valueLower);
    const queryPosB = bLower.indexOf(valueLower);
    if (queryPosA !== queryPosB) {
      return queryPosA - queryPosB;
    }
    return aLower < bLower ? -1 : 1;
  }

  static getProducts() {
    return AutocompleteUtils.data;
  }

  static getProductByName(name) {
    return AutocompleteUtils.data.find(product => product.name === name);
  }
}

export default AutocompleteUtils;
