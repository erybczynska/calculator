import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  setCarbs,
  setFats,
  setProteins
} from '../../../redux/macroActions';


class MacroForm extends Component {

  constructor(props) {
    super(props);

    this.handleFatsInput = this.handleFatsInput.bind(this);
    this.handleProteinsInput = this.handleProteinsInput.bind(this);
    this.handleCarbsInput = this.handleCarbsInput.bind(this);
    this.checkValueFats = this.checkValueFats.bind(this);
    this.checkValueProteins = this.checkValueProteins.bind(this);
    this.checkValueCarbs = this.checkValueCarbs.bind(this);
  }

  handleFatsInput(event) {
    this.checkValueFats(event.target.value);
    this.props.dispatch(setFats(event.target.value));
  }

  handleProteinsInput(event) {
    this.checkValueProteins(event.target.value);
    this.props.dispatch(setProteins(event.target.value));
  }

  handleCarbsInput(event) {
    this.checkValueCarbs(event.target.value);
    this.props.dispatch(setCarbs(event.target.value));
  }

  checkValueFats(fats) {
    let proteins = this.props.proteinsInput;
    let carbs = this.props.carbsInput;

    if (fats + proteins + carbs !== 100) {
      if (carbs > proteins) {
        this.props.dispatch(setCarbs(100 - proteins - fats));
      } else {
        this.props.dispatch(setProteins(100 - carbs - fats));
      }
    }
  }

  checkValueProteins(proteins) {
    let fats = this.props.fatsInput;
    let carbs = this.props.carbsInput;

    if (fats + proteins + carbs !== 100) {
      if (fats > carbs) {
        this.props.dispatch(setFats(100 - carbs - proteins));
      } else {
        this.props.dispatch(setCarbs(100 - fats - proteins));
      }
    }
  }

  checkValueCarbs(carbs) {
    let proteins = this.props.proteinsInput;
    let fats = this.props.fatsInput;

    if (fats + proteins + carbs !== 100) {
      if (fats > proteins) {
        this.props.dispatch(setFats(100 - carbs - proteins));
      } else {
        this.props.dispatch(setProteins(100 - carbs - fats));
      }
    }
  }

  render() {
    return (
        <form>
          <label>
            Fats:<br />
            <input type="number" name="quantity"
                   value={this.props.fatsInput} min="0" max="100"
                   onChange={this.handleFatsInput}/><br />
            Protein:<br />
            <input type="number" name="quantity"
                   value={this.props.proteinsInput} min="0" max="100"
                   onChange={this.handleProteinsInput}/><br />
            Carbs:<br />
            <input type="number" name="quantity"
                   value={this.props.carbsInput} min="0" max="100"
                   onChange={this.handleCarbsInput}/><br />
            <input type="submit" value="Count"/>
          </label>
        </form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    carbsInput: state.macroApp.carbsInput,
    proteinsInput: state.macroApp.proteinsInput,
    fatsInput: state.macroApp.fatsInput
  }
};

const Macro = connect(mapStateToProps)(MacroForm);

export default Macro;
