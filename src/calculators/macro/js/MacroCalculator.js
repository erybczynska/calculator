import React, {Component} from 'react';
import '../style/MacroCalculator.css';
import {Grid, Col, Row} from 'react-bootstrap';
import MacroGraph from './MacroGraph';
import MacroTable from './MacroTable';
import MacroForm from './MacroForm';

class MacroCalculator extends Component {

  render() {
    return (
        <main>
          <div className="MacroCalculator">
            <Grid>
              <Row className="show-grid">
                <h1>Protein, Carbs & Fat</h1>
              </Row>
              <Row className="show-grid">
                <Col md={4}>
                  <MacroForm/>
                </Col>
                <Col md={4}>
                  <MacroGraph/>
                </Col>
                <Col md={4}>
                  <MacroTable/>
                </Col>
              </Row>
            </Grid>
          </div>
        </main>
    );
  }
}

export default MacroCalculator;