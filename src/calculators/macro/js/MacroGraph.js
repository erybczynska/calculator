import React, {Component} from 'react';
import '../style/MacroGraph.css';
import {PieChart} from 'react-easy-chart';
import {connect} from 'react-redux';

class MacroGraph extends Component {

  render() {
    const fats = this.props.fatsInput;
    const proteins = this.props.proteinsInput;
    const carbs = this.props.carbsInput;

    return (
        <div className="Graph">
          <PieChart
              labels styles={{
            '.chart_lines': {
              strokeWidth: 0
            },
            '.chart_text': {
              fontSize: '1.25em',
              fill: '#fff',
            }
          }}
              size={300}
              data={[
                {key: 'Fats', value: checkNaN(fats), color: '#7CC249'},
                {key: 'Proteins', value: checkNaN(proteins), color: '#F49643'},
                {key: 'Carbs', value: checkNaN(carbs), color: '#F5055A'}
              ]}
          />
        </div>
    );
  }
}

function checkNaN(value) {
  if (isNaN(value)) {
    return 1;
  }
  return value;
}

const mapStateToProps = (state) => {
  return {
    fatsInput: state.macroApp.fatsInput,
    proteinsInput: state.macroApp.proteinsInput,
    carbsInput: state.macroApp.carbsInput
  }
};

const Macro = connect(mapStateToProps)(MacroGraph);

export default Macro;

