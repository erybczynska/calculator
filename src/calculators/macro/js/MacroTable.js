import React, {Component} from 'react';
import '../style/MacroTable.css';
import {Table} from 'react-bootstrap';
import {connect} from 'react-redux';
import {calculateIntake} from '../../intake/js/calculator';

class MacroTable extends Component {
  render() {
    const fats = this.props.fatsInput;
    const protein = this.props.proteinsInput;
    const carbs = this.props.carbsInput;

    const intake = calculateIntake(this.props.data);
    const calorieIntake = intake.caloriesPerDay;
    const fatCal = 9;
    const carbCal = 4;
    const proteinCal = 4;

    let proteinGrams = calculateMacroGrams(calorieIntake,
        protein, proteinCal);

    let fatsGrams = calculateMacroGrams(calorieIntake,
        fats, fatCal);

    let carbsGrams = calculateMacroGrams(calorieIntake,
        carbs, carbCal);

    return (
        <div className="MacroTable">
          <h3>Calorie intake: {!isNaN(calorieIntake) && calorieIntake > 0 &&
          <strong>{calorieIntake} kcal</strong>} </h3>
          <Table responsive>
            <thead>
            <tr>
              <th>Protein</th>
              <th>Carbs</th>
              <th>Fat</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <th>{!isNaN(protein) && protein > 0 &&
              <span>{protein}%</span>}
              </th>

              <th>{!isNaN(carbs) && carbs > 0 &&
              <span>{carbs}%</span>}
              </th>

              <th>{!isNaN(fats) && fats > 0 &&
              <span>{fats}%</span>}
              </th>

            </tr>
            <tr>
              <th>{proteinGrams > 0 && <span>{proteinGrams} g</span>}
              </th>
              <th>{carbsGrams > 0 && <span>{carbsGrams} g</span>}
              </th>
              <th>{fatsGrams > 0 && <span>{fatsGrams} g</span>}
              </th>
            </tr>
            </tbody>
          </Table>
        </div>
    );
  }
}

function calculateMacroGrams(intake, macro, macroCal) {
  return Math.round((intake * macro / 100) / macroCal);
}

const mapStateToProps = (state) => {
  return {
    data: {
      weightInput: state.personDataApp.weightInput,
      ageInput: state.personDataApp.ageInput,
      heightInput: state.personDataApp.heightInput,
      genderInput: state.personDataApp.genderInput,
      activityInput: state.personDataApp.activityInput
    },
    fatsInput: state.macroApp.fatsInput,
    proteinsInput: state.macroApp.proteinsInput,
    carbsInput: state.macroApp.carbsInput
  }
};

const Macro = connect(mapStateToProps)(MacroTable);

export default Macro;
