import React, {Component} from 'react';
import {connect} from 'react-redux';
import {calculateIntake} from './calculator';

class Calories extends Component {

  render() {

    const caloriesIntake = calculateIntake(this.props.data);
    return (
        <div className="Calories">
          <h3><strong>BMR:</strong> {!isNaN(caloriesIntake.bmr) &&
          <span>{Math.round(caloriesIntake.bmr)} kcal</span>}</h3>
          <h3><strong>Your calorie
            intake:</strong> {!isNaN(caloriesIntake.caloriesPerDay) &&
          <span>{Math.round(caloriesIntake.caloriesPerDay)} kcal</span>}</h3>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: {
      weightInput: state.personDataApp.weightInput,
      ageInput: state.personDataApp.ageInput,
      heightInput: state.personDataApp.heightInput,
      genderInput: state.personDataApp.genderInput,
      activityInput: state.personDataApp.activityInput
    }
  }
};

const Intake = connect(mapStateToProps)(Calories);

export default Intake;
