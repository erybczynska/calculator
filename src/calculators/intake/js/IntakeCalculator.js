import React, {Component} from 'react';
import Calories from './Calories';
import PersonDataForm from './PersonDataForm';
import PersonDataRadioForm from './PersonDataRadioForm';
import {Grid, Col, Row} from 'react-bootstrap';

class IntakeCalculator extends Component {

  render() {
    return (
        <main>
          <div className="IntakeCalculator" id="calories">
            <Grid>
              <Row className="show-grid">
                <h1>How many calories do you need?</h1>
              </Row>
              <Row className="show-grid">
                <Col md={4}>
                  <PersonDataForm />
                </Col>
                <Col md={4}>
                  <PersonDataRadioForm />
                </Col>
                <Col md={4}>
                  <Calories/>
                </Col>
              </Row>
            </Grid>
          </div>
        </main>
    );
  }
}

export default IntakeCalculator;