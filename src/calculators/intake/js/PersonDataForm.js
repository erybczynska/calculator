import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  setPersonWeight,
  setPersonAge,
  setPersonHeight
} from '../../../redux/personDataActions';

class PersonDataForm extends Component {

  constructor(props) {
    super(props);

    this.handleWeightInput = this.handleWeightInput.bind(this);
    this.handleAgeInput = this.handleAgeInput.bind(this);
    this.handleHeightInput = this.handleHeightInput.bind(this);
  }

  handleWeightInput(event) {
    this.props.dispatch(setPersonWeight(event.target.value));
  }

  handleAgeInput(event) {
    this.props.dispatch(setPersonAge(event.target.value));
  }

  handleHeightInput(event) {
    this.props.dispatch(setPersonHeight(event.target.value));
  }

  render() {
    return (
        <form>
          <label>
            Weight:<br />
            <input type="number"
                   value={this.props.weightInput}
                   onChange={this.handleWeightInput}
                   min="1" max="600"/><br />
            Age:<br />
            <input type="number" value={this.props.ageInput}
                   onChange={this.handleAgeInput}
                   min="1" max="150"/><br />
            Height:<br />
            <input type="number" value={this.props.heightInput}
                   onChange={this.handleHeightInput}
                   min="20" max="250"/><br />
          </label>
        </form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    weightInput: state.weightInput,
    ageInput: state.ageInput,
    heightInput: state.heightInput
  }
};

const Intake = connect(mapStateToProps)(PersonDataForm);

export default Intake;



