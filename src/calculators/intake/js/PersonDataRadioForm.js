import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  setPersonActivity,
  setPersonGender
} from '../../../redux/personDataActions';
import {RadioGroup, Radio} from 'react-radio-group';

class PersonDataRadioForm extends Component {

  constructor(props) {
    super(props);

    this.handleGenderInput = this.handleGenderInput.bind(this);
    this.handleActivityInput = this.handleActivityInput.bind(this);
  }

  handleGenderInput(value) {
    this.props.dispatch(setPersonGender(value));
  }

  handleActivityInput(value) {
    this.props.dispatch(setPersonActivity(value));
  }

  render() {
    return (
        <div className="PersonDataRadioForm">
          <label className="radio-label">
            <RadioGroup name="gender"
                        selectedValue={this.props.genderInput}
                        onChange={this.handleGenderInput}>
              Gender: <br />
              <label><Radio value="-161"/> Female</label><br />
              <label><Radio value="5"/> Male</label><br />
            </RadioGroup>
          </label>

          <label className="radio-label">
            <RadioGroup name="activity"
                        selectedValue={this.props.activityInput}
                        onChange={this.handleActivityInput}>
              Activity: <br />
              <div className="option">
                <label><Radio value="1.2"/>
                <mark>No activity or very low activity</mark></label>
                <br />
              </div>
              <div className="option">
              <label><Radio value="1.375"/>
                <mark>Light physical activity</mark>
                : light
                workouts 1-3 times a week</label><br />
              </div>
              <div className="option">
                <label><Radio value="1.55"/>
                <mark>Average physical activity</mark>
                : 3-5 workouts
                per week of moderate intensity</label><br />
              </div>
              <div className="option">
                <label><Radio value="1.725"/>
                <mark>High physical activity</mark>
                : 6-7 workouts a medium or high intensity</label><br />
              </div>
            </RadioGroup>
          </label>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      genderInput: state.genderInput,
      activityInput: state.activityInput
    }
};

const Intake = connect(mapStateToProps)(PersonDataRadioForm);

export default Intake;
