export const calculateIntake = (data) => {
  const weight = parseFloat(data.weightInput);
  const age = parseFloat(data.ageInput);
  const height = parseFloat(data.heightInput);
  const gender = parseFloat(data.genderInput);
  const activity = parseFloat(data.activityInput);

  const bmr = (10 * weight) + (6.25 * height) - (5 * age) + gender;
  const caloriesPerDay = activity * bmr;

  return {
    bmr: bmr,
    caloriesPerDay: caloriesPerDay
  }
};
