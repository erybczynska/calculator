import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import {
  BrowserRouter as Router,
  Switch
} from 'react-router-dom';
import calculatorApp from './redux/calculatorApp';
import App from './main/js/App';
import './index.css';

let store = createStore(calculatorApp);

ReactDOM.render(
    <Provider store={store}>
      <Router>
        <Switch>
          <App/>
        </Switch>
      </Router>
    </Provider>,
    document.getElementById('root')
);
