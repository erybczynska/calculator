import React, {Component} from 'react';
import '../style/App.css';
import {Route} from 'react-router-dom';
import Header from '../../header/js/Header';
import Wrapper from '../../wrapper/js/Wrapper';
import Footer from '../../footer/js/Footer';
import AboutCalculator from '../../about/js/AboutCalculator';
import IntakeCalculator from '../../calculators/intake/js/IntakeCalculator';
import ProductsCalculator from '../../calculators/products/js/ProductsCalculator';
import MacroCalculator from '../../calculators/macro/js/MacroCalculator';

class App extends Component {

  render() {
    return (
        <div className="App">
          <Header/>
          <Wrapper/>
          <div className='Content'>
            <Route exact path="/" component={AboutCalculator}/>
            <Route path="/calories" component={IntakeCalculator}/>
            <Route path="/products" component={ProductsCalculator}/>
            <Route path="/macro" component={MacroCalculator}/>
          </div>
          <Footer/>
        </div>
    );
  }
}

export default App;


