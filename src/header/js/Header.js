import React, {Component} from 'react';
import '../style/Header.css';
import {Link} from 'react-router-dom'
import {Grid, Col, Row} from 'react-bootstrap';
import Scrollchor from 'react-scrollchor';

class Header extends Component {
  render() {
    return (
        <header>
          <div className="Header">
            <Grid>
              <Row className="show-grid">
                <Col md={4}>
                  <h1>Calculator</h1>
                </Col>
                <Col md={8}>
                  <nav>
                    <ul className="nav nav-pills">
                      <li><Link to="/">About</Link></li>
                      <li><Link to="/calories">Calories</Link></li>
                      <li><Link to="/macro">Macro</Link></li>
                      <li><Link to="/products">Products</Link></li>
                    </ul>
                  </nav>
                </Col>
              </Row>
            </Grid>
          </div>
        </header>
    );
  }
}

export default Header;