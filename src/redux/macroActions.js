export const SET_PROTEINS = 'SET_PROTEINS';
export const SET_CARBS = 'SET_CARBS';
export const SET_FATS = 'SET_FATS';

export const setProteins = (proteinsInput) => {
  return {
    type: SET_PROTEINS,
    proteinsInput: proteinsInput
  }
};

export const setCarbs = (carbsInput) => {
  return {
    type: SET_CARBS,
    carbsInput: carbsInput
  }
};

export const setFats = (fatsInput) => {
  return {
    type: SET_FATS,
    fatsInput: fatsInput
  }
};
