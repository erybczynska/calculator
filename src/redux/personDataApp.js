import {combineReducers} from 'redux'
import {
  SET_PERSON_WEIGHT,
  SET_PERSON_AGE,
  SET_PERSON_HEIGHT,
  SET_PERSON_GENDER,
  SET_PERSON_ACTIVITY
} from './personDataActions';

function weightInput(state = '', action) {
  switch (action.type) {
    case SET_PERSON_WEIGHT:
      return action.weightInput;
    default:
      return state
  }
}

function ageInput(state = '', action) {
  switch (action.type) {
    case SET_PERSON_AGE:
      return action.ageInput;
    default:
      return state
  }
}

function heightInput(state = '', action) {
  switch (action.type) {
    case SET_PERSON_HEIGHT:
      return action.heightInput;
    default:
      return state
  }
}

function genderInput(state = '', action) {
  switch (action.type) {
    case SET_PERSON_GENDER:
      return action.genderInput;
    default:
      return state
  }
}

function activityInput(state = '', action) {
  switch (action.type) {
    case SET_PERSON_ACTIVITY:
      return action.activityInput;
    default:
      return state
  }
}

const personDataApp = combineReducers({
  weightInput,
  ageInput,
  heightInput,
  genderInput,
  activityInput
});

export default personDataApp;