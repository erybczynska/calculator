import {combineReducers} from 'redux';
import {
  SET_CARBS,
  SET_PROTEINS,
  SET_FATS
} from './macroActions';

function carbsInput(state = '50', action) {
  switch (action.type) {
    case SET_CARBS:
      return action.carbsInput;
    default:
      return state
  }
}

function proteinsInput(state = '30', action) {
  switch (action.type) {
    case SET_PROTEINS:
      return action.proteinsInput;
    default:
      return state
  }
}

function fatsInput(state = '20', action) {
  switch (action.type) {
    case SET_FATS:
      return action.fatsInput;
    default:
      return state
  }
}

const macroApp = combineReducers({
  carbsInput,
  proteinsInput,
  fatsInput
});

export default macroApp;