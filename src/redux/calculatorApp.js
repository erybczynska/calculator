import {combineReducers} from 'redux';
import macroApp from './macroApp';
import personDataApp from './personDataApp';

const calculatorApp = combineReducers({
  personDataApp,
  macroApp
});

export default calculatorApp;
