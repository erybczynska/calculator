export const SET_PERSON_WEIGHT = 'SET_PERSON_WEIGHT';
export const SET_PERSON_AGE = 'SET_PERSON_AGE';
export const SET_PERSON_HEIGHT = 'SET_PERSON_HEIGHT';
export const SET_PERSON_GENDER = 'SET_PERSON_GENDER';
export const SET_PERSON_ACTIVITY = 'SET_PERSON_ACTIVITY';

export const setPersonWeight = (weightInput) => {
  return {
    type: SET_PERSON_WEIGHT,
    weightInput: weightInput
  }
};

export const setPersonAge = (ageInput) => {
  return {
    type: SET_PERSON_AGE,
    ageInput: ageInput
  }
};

export const setPersonHeight = (heightInput) => {
  return {
    type: SET_PERSON_HEIGHT,
    heightInput: heightInput
  }
};

export const setPersonGender = (genderInput) => {
  return {
    type: SET_PERSON_GENDER,
    genderInput: genderInput
  }
};

export const setPersonActivity = (activityInput) => {
  return {
    type: SET_PERSON_ACTIVITY,
    activityInput: activityInput
  }
};
