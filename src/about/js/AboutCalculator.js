import React, {Component} from 'react';
import '../style/About.css';
import {Grid, Col, Row} from 'react-bootstrap';

class AboutCalculator extends Component {
  render() {
    return (
        <main>
          <div className="About">
            <Grid>
              <Row className="show-grid">
                <Col md={4}>
                  <h1>Calories</h1>
                  <p>Calculate your caloric intake. You will learn how much your
                    body needs calories per day -
                    <mark>BMR (Basal Metabolic Rate)</mark>
                    with Mifflin - St Jeor
                    Formula.
                  </p>
                </Col>
                <Col md={4}>
                  <h1>Macro</h1>
                  <p>Calculate your macronutrients intake. There are three major
                    macronutrients that the human body needs in order to
                    function
                    properly:
                    <mark>carbohydrates, protein, and fats</mark>
                    .
                  </p>
                </Col>
                <Col md={4}>
                  <h1>Products</h1>
                  <p>Calculate
                    <mark>how many calories</mark>
                    you ate in your meal.
                    You will learn how much of each macronutrient have consumed
                    during the day.
                  </p>
                </Col>
              </Row>
            </Grid>
          </div>
        </main>
    );
  }
}

export default AboutCalculator;